import colorsys

def hex_to_rgb(h):
    h = h.strip("#")
    return int(h[0:2], 16)/255, int(h[2:4], 16)/255, int(h[4:6], 16)/255

def hex_to_light_pair(hex_val, lightening=0.6):
    h, l, s = colorsys.rgb_to_hls(*hex_to_rgb(hex_val))
    l = 1-((1-l) * lightening)
    lighter = colorsys.hls_to_rgb(h, l, s)
    lighter_int = [int(v*255) for v in lighter]
    return hex_val, f"#{lighter_int[0]:x}{lighter_int[1]:x}{lighter_int[2]:x}"
   


import svgwrite

def add_element(d, insert, size, text):
    e = d.rect(insert, size)
    e["class"] = "element"
    d.add(e)
    t = d.text(text, (insert[0] + size[0]/2, insert[1] + size[1]/2))
    t["class"] = "elem_text"
    d.add(t)

def add_bbox(d):
    b = d.rect((0, 0), ("100%", "100%"), style="stroke: black; stroke-width: 1px; fill-opacity: 0.0")
    d.add(b)
    
def add_array(d, a, insert, size):
    for i, v in enumerate(a):
        add_element(d, (insert[0], insert[1]+size[1]*i), size, str(v))
    return insert, (size[0], size[1]*len(a))
        
def add_arrow(d, insert, width, length, head_width, head_length):
    raw = [
        (0, -width/2), (length, -width/2),
        (length, -width/2-head_width), (length+head_length, 0), (length, +width/2+head_width),
        (length, width/2), (0, width/2)
    ]
    moved = [(x+insert[0], y+insert[1]) for x, y in raw]
    a = d.polygon(moved)
    a["class"] = "arrow"
    d.add(a)
    
def add_code(d, insert, text):
    if isinstance(text, str):
        c = d.text(text, insert)
    else:
        c = d.text("", insert)
        for t, klass in text:
            s = c.add(d.tspan(t))
            if klass is not None:
                s["class"] = klass
    c["class"] = "code"
    d.add(c)
    
def add_header(d, insert, text):
    c = d.text(text, insert)
    c["class"] = "header"
    d.add(c)


import numpy as np

e_c, e_l = hex_to_light_pair("#70c3e6")
a_c, a_l = hex_to_light_pair("#000000")

dwg = svgwrite.Drawing(size=(400, 150))
dwg.defs.add(dwg.style(f"""
text {{
    dominant-baseline: middle;
    font-size: 16px;
}}
.element {{
    fill: {e_l};
    stroke: {e_c};
    stroke-width: 3px;
}}
.elem_text {{
    text-anchor: middle;
}}
.arrow {{
    fill: {a_l};
    stroke: {a_c};
    stroke-width: 2px;
}}
.code {{
    font-family: mono;
}}
tspan.highlight {{
    fill: #70c3e6;
}}
"""))
top_offset = 10
_, elements_size = add_array(dwg, np.array([1, 2, 3]), (290, top_offset), (80, 40))
add_arrow(dwg, (210, elements_size[1]/2+top_offset), 15, 30, 5, 15)
add_code(dwg, (20, elements_size[1]/2+top_offset), [("np.array(", None), ("[1, 2, 3]", "highlight"), (")", None)])
add_bbox(dwg)
dwg.saveas("test.svg")
#print(svgwrite.utils.pretty_xml(dwg.tostring()))
dwg



import numpy as np

e_c, e_l = hex_to_light_pair("#70c3e6")
a_c, a_l = hex_to_light_pair("#000000")

dwg = svgwrite.Drawing(size=(370, 150))
dwg.defs.add(dwg.style(f"""
text {{
    dominant-baseline: middle;
    font-size: 16px;
}}
.element {{
    fill: {e_l};
    stroke: {e_c};
    stroke-width: 3px;
}}
.elem_text {{
    text-anchor: middle;
}}
.arrow {{
    fill: {a_l};
    stroke: {a_c};
    stroke-width: 2px;
}}
.code {{
    font-family: mono;
}}
tspan.highlight {{
    fill: #70c3e6;
}}
"""))
top_offset = 10
_, elements_size = add_array(dwg, np.array([3.14, 2.71, 1.18]), (10, top_offset), (80, 40))
add_code(dwg, (120, elements_size[1]/2+top_offset), "×")
add_code(dwg, (150, elements_size[1]/2+top_offset), "2")
add_arrow(dwg, (190, elements_size[1]/2+top_offset), 15, 30, 5, 15)
add_array(dwg, np.array([6.28, 5.42, 2.36]), (270, top_offset), (80, 40))
add_bbox(dwg)
dwg.saveas("array_multiply.svg")
#print(svgwrite.utils.pretty_xml(dwg.tostring()))
dwg
