#!/usr/bin/env python
# coding: utf-8

from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt

# Rainfall data

import ecmwf.data as ecdata
from ecmwf.opendata import Client

client = Client("ecmwf", beta=True)

parameters = ['tp']
date = "2022-04-03 12:00:00"
rain_filename = f'medium-rain-acc-{date.replace(" ", "")}.grib'

if not Path(rain_filename).exists():
    client.retrieve(
        date=date,
        step=24,
        stream="oper",
        type="fc",
        levtype="sfc",  # surface
        param=parameters,
        target=rain_filename
    )

rain = ecdata.read(rain_filename)  # units: m

lats = rain.latitudes()
u, indices = np.unique(lats, return_index=True)
lats = lats[indices] * -1

lons = rain.longitudes()
u, indices = np.unique(lons, return_index=True)
lons = lons[indices]

vals = rain.values()
rain = vals.reshape(len(lats), (len(lons)))

select_lat = (35 < lats) & (lats < 65)
select_lon = (-20 < lons) & (lons < 10)
uk_rain = rain[select_lat, :]
uk_rain = uk_rain[:, select_lon]


fig, ax = plt.subplots()
ax.imshow(uk_rain)


# Temperature data

parameters = ['t']
date = "2022-04-03 12:00:00"
temp_filename = f'medium-t-z-{date.replace(" ", "")}.grib'

level_list = [1000,925,850,700,500,300,250,200,50]

if not Path(temp_filename).exists():
    client.retrieve(
        date=date,
        #time=0,
        step=12,
        stream="oper",
        type="fc",
        levtype="pl",  # pressure levels
        levelist=level_list,
        param=parameters,
        target=temp_filename
    )

temp = ecdata.read(temp_filename)["t"]  # units: K

lats = temp.latitudes()[0]
u, indices = np.unique(lats, return_index=True)
lats = lats[indices] * -1

lons = temp.longitudes()[0]
u, indices = np.unique(lons, return_index=True)
lons = lons[indices]

temp = temp.values().reshape(len(level_list), len(lats), len(lons))

select_lat = (35 < lats) & (lats < 65)
select_lon = (-20 < lons) & (lons < 10)
uk_temp = temp[:, select_lat, :]
uk_temp = uk_temp[:, :, select_lon]
uk_temp = uk_temp[1:]

fig, ax = plt.subplots()
im = ax.imshow(uk_temp[0]-273, cmap="inferno")
plt.colorbar(im, ax=ax)


print(np.max(uk_temp[0]) - 273.15)

fig, ax = plt.subplots()
ax.plot(uk_temp[2, :, 0] - 273.15)


# Wind speed data

client = Client("ecmwf", beta=True)

date = "2022-04-03 12:00:00"
wind_filename = f'medium-10m-probability-{date.replace(" ", "")}.grib'

if not Path(wind_filename).exists():
    client.retrieve(
        date=date,
        #time=0,
        step=24,
        stream="enfo",
        type=['pf'],
        number=[2],
        levtype="sfc",
        param=["10u","10v"],
        target=wind_filename
    )

wind = ecdata.read(wind_filename)


wind_v = wind["10v"]  # northward, units: m/s
wind_v.describe()

lats = wind_v.latitudes()
u, indices = np.unique(lats, return_index=True)
lats = lats[indices] * -1

lons = wind_v.longitudes()
u, indices = np.unique(lons, return_index=True)
lons = lons[indices]

vals_v = wind_v.values()
wind_v = vals_v.reshape(len(lats), (len(lons)))

select_lat = (35 < lats) & (lats < 65)
select_lon = (-20 < lons) & (lons < 10)
uk_wind_v = wind_v[select_lat, :]
uk_wind_v = uk_wind_v[:, select_lon]


wind_u = wind["10u"]  # eastward, units: m/s
wind_u.describe()

lats = wind_u.latitudes()
u, indices = np.unique(lats, return_index=True)
lats = lats[indices] * -1

lons = wind_u.longitudes()
u, indices = np.unique(lons, return_index=True)
lons = lons[indices]

vals_u = wind_u.values()
wind_u = vals_u.reshape(len(lats), (len(lons)))

select_lat = (35 < lats) & (lats < 65)
select_lon = (-20 < lons) & (lons < 10)
uk_wind_u = wind_u[select_lat, :]
uk_wind_u = uk_wind_u[:, select_lon]


uk_wind_speed = np.sqrt(uk_wind_u ** 2 + uk_wind_v ** 2)
uk_wind_dir = np.arctan2(uk_wind_u, uk_wind_v)


fig, ax = plt.subplots(2, 2, figsize=(10,10), constrained_layout=True)
im_u = ax[0][0].imshow(uk_wind_u, cmap="coolwarm")
im_v = ax[0][1].imshow(uk_wind_v, cmap="coolwarm")
im_s = ax[1][0].imshow(uk_wind_speed)
im_d = ax[1][1].imshow(uk_wind_dir, cmap="twilight")
fig.colorbar(im_u, ax=ax[0][0])
fig.colorbar(im_v, ax=ax[0][1])
fig.colorbar(im_s, ax=ax[1][0])
fig.colorbar(im_d, ax=ax[1][1])


fig, ax = plt.subplots(figsize=(10, 10))
ax.quiver(uk_wind_u[::-1], uk_wind_v, uk_wind_speed)


fig, ax = plt.subplots(figsize=(10, 10))
X, Y = np.meshgrid(np.linspace(-20, 10, uk_wind_u.shape[0]), np.linspace(35, 65, uk_wind_u.shape[1]))                                        
ax.streamplot(X, Y, uk_wind_u[::-1], uk_wind_v, linewidth=uk_wind_speed/10, density=2)


# Region mask

import regionmask

masks = regionmask.defined_regions.natural_earth_v5_0_0.countries_110.mask_3D(lons[select_lon], lats[select_lat])
uk_mask = masks.isel(region=(masks.abbrevs == "GB"))[0].values
irl_mask = masks.isel(region=(masks.abbrevs == "IRL"))[0].values
f_mask = masks.isel(region=(masks.abbrevs == "F"))[0].values
e_mask = masks.isel(region=(masks.abbrevs == "E"))[0].values

fig, ax = plt.subplots()
ax.imshow(np.where(uk_mask|f_mask|irl_mask|e_mask, uk_rain, np.nan))


# 1D data

import pandas as pd

rain_df = pd.read_csv("https://milliams.com/courses/data_analysis_python/rain.csv")
one_d_rain = rain_df["Oxford"].values
one_d_rain_index = rain_df.index
fig, ax = plt.subplots()
ax.plot(one_d_rain_index, one_d_rain)


# Saving out data

np.savez_compressed(
    "weather_data",
    rain=uk_rain,
    temperature=uk_temp,
    wind_u=uk_wind_u,
    wind_v=uk_wind_v,
    uk=uk_mask,
    ireland=irl_mask,
    spain=e_mask,
    france=f_mask,
    rain_history=one_d_rain,
    rain_history_index=one_d_rain_index,
)

# Test it

with np.load("weather_data.npz") as foo:
    uk_rain = foo["rain"]

