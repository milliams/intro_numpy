{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6764cf9b-0bde-45cb-be43-43aa064b1b41",
   "metadata": {},
   "source": [
    "# Filtering data\n",
    "\n",
    "Manaully selecting data from an array by giving indices or ranges works if you want contiguous chunks of data, but sometimes you want to be able to grab arbitrary data from an array.\n",
    "\n",
    "Let's explore the options for more advanced indexing of arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "a829a2d3-4601-431b-bb73-b8c0069b56bb",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac63a0cd-e5dd-47ce-a3da-4230026f9ecd",
   "metadata": {},
   "source": [
    "We'll use the following array in our examples. To make it easier to understand what's going on, I've set the value of the digit after the decimal place to match the index (i.e. the value at index <u>3</u> is 10.<u>3</u>):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "e593f640-1dcb-4148-8cdd-2c3af7679603",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array([6.0, 2.1, 8.2, 10.3, 5.4, 1.5, 7.6, 3.7, 9.8])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "004cab5e-9a2c-4cf8-90c2-398d1d6c789f",
   "metadata": {},
   "source": [
    "## Filter by boolean\n",
    "\n",
    "Let's say that we want all the values in the array that are larger than 4.0. We could do this by manually finding all those indices which match and constructing a list of them to use as a selector:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "99a54065-13c6-4617-bdfe-d5895a148399",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 6. ,  8.2, 10.3,  5.4,  7.6,  9.8])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "large_indices = [0, 2, 3, 4, 6, 8]\n",
    "a[large_indices]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a495fac-3a54-4adb-b904-58737aabae62",
   "metadata": {},
   "source": [
    "Or, we can use a list of boolean values, where we set those elements we want to extract to `True` and those we want to be rid of to `False`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "96b4246f-4723-4c90-907f-4cf88998244d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 6. ,  8.2, 10.3,  5.4,  7.6,  9.8])"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mask = [True, False, True, True, True, False, True, False, True]\n",
    "a[mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f7126a0-d878-4f89-adc7-72c800abf14e",
   "metadata": {},
   "source": [
    "These lists of `True` and `False` are referred to as *boolean arrays*.\n",
    "\n",
    "With a larger array it would be tedious to create this list by hand. Luckily NumPy provides us with a way of constructing these automatically. If we want a boolean array matching the values which are greater than 4, we can use the same sort of syntax we used for multiplication, but use `>` instead:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "641dfdde-6836-4a35-80fa-e4faeca5a443",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ True, False,  True,  True,  True, False,  True, False,  True])"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a > 4"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3dfccc13-2833-4601-bd72-b63f52a74865",
   "metadata": {},
   "source": [
    "Or, diagramatically (using ■ for `True` and □ for `False`):"
   ]
  },
  {
   "cell_type": "raw",
   "id": "935b3bfe-06eb-4e66-82d5-86fbcb09dcd0",
   "metadata": {},
   "source": [
    "<div class=\"operation\">\n",
    "<div>\n",
    "    <table class=\"array\"><tr><td>&nbsp;6.0</td></tr><tr><td>&nbsp;2.1</td></tr><tr><td>&nbsp;8.2</td></tr><tr><td>10.3</td></tr><tr><td>&nbsp;5.4</td></tr><tr><td>&nbsp;1.5</td></tr><tr><td>&nbsp;7.6</td></tr><tr><td>&nbsp;3.7</td></tr><tr><td>&nbsp;9.8</td></tr></table>\n",
    "</div>\n",
    "<div>&gt;</div>\n",
    "<div>4</div>\n",
    "<span style=\"font-size: 200%;\">⇨</span>\n",
    "<div>\n",
    "    <table class=\"array\"><tr><td>■</td></tr><tr><td>□</td></tr><tr><td>■</td></tr><tr><td>■</td></tr><tr><td>■</td></tr><tr><td>□</td></tr><tr><td>■</td></tr><tr><td>□</td></tr><tr><td>■</td></tr></table>\n",
    "</div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42d7283d-b672-47d3-a967-5a6ee3e07f78",
   "metadata": {},
   "source": [
    "This mask can be saved to a variable and passed in as an index:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "27be027e-3edd-4862-b1a1-2f885cb04394",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 6. ,  8.2, 10.3,  5.4,  7.6,  9.8])"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mask = a > 4\n",
    "a[mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1aa2d69c-47bd-477b-af96-4aacc4a63f10",
   "metadata": {},
   "source": [
    "Or, in one line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "cb810987-bebd-422f-b11f-31c7518ff9ba",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 6. ,  8.2, 10.3,  5.4,  7.6,  9.8])"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a[a > 4]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "feb29160-3cde-414a-bf4b-a574c9c92d80",
   "metadata": {},
   "source": [
    "which can be read as \"select from `a` the elements where `a` is greater than 4\""
   ]
  },
  {
   "cell_type": "raw",
   "id": "a684a21f-ed62-4a6a-8a1e-a512a424981b",
   "metadata": {},
   "source": [
    "<div class=\"operation\">\n",
    "<div>\n",
    "    <table class=\"array\"><tr><td>&nbsp;6.0</td></tr><tr><td>&nbsp;2.1</td></tr><tr><td>&nbsp;8.2</td></tr><tr><td>10.3</td></tr><tr><td>&nbsp;5.4</td></tr><tr><td>&nbsp;1.5</td></tr><tr><td>&nbsp;7.6</td></tr><tr><td>&nbsp;3.7</td></tr><tr><td>&nbsp;9.8</td></tr></table>\n",
    "</div>\n",
    "<div>[</div>\n",
    "<div>\n",
    "    <table class=\"array\"><tr><td>■</td></tr><tr><td>□</td></tr><tr><td>■</td></tr><tr><td>■</td></tr><tr><td>■</td></tr><tr><td>□</td></tr><tr><td>■</td></tr><tr><td>□</td></tr><tr><td>■</td></tr></table>\n",
    "</div>\n",
    "<div>]</div>\n",
    "<span style=\"font-size: 200%;\">⇨</span>\n",
    "<div>\n",
    "    <table class=\"array\"><tr><td>&nbsp;6.0</td></tr><tr><td class=\"empty\">&nbsp;</td></tr><tr><td>&nbsp;8.2</td></tr><tr><td>10.3</td></tr><tr><td>&nbsp;5.4</td></tr><tr><td class=\"empty\">&nbsp;</td></tr><tr><td>&nbsp;7.6</td></tr><tr><td class=\"empty\">&nbsp;</td></tr><tr><td>&nbsp;9.8</td></tr></table>\n",
    "</div>\n",
    "<span style=\"font-size: 200%;\">⇨</span>\n",
    "<div>\n",
    "    <table class=\"array\"><tr><td>&nbsp;6.0</td></tr><tr><td>&nbsp;8.2</td></tr><tr><td>10.3</td></tr><tr><td>&nbsp;5.4</td></tr><tr><td>&nbsp;7.6</td></tr><tr><td>&nbsp;9.8</td></tr></table>\n",
    "</div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6300206e-a4b3-4c74-92e4-4dca39517ade",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Practice\n",
    "\n",
    "Extract all the values in `a` which are:\n",
    "1. Less than 5\n",
    "2. Greater than or equal to 8.2\n",
    "3. Equal to 6.0\n",
    "\n",
    "[<small>answer</small>](answer_filtering_compare.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac518a69-aeed-4020-87a3-1c2241b72f90",
   "metadata": {},
   "source": [
    "## Setting from filters\n",
    "\n",
    "Just like at the beginning of the course when we set values in an array with:\n",
    "```python\n",
    "a[4] = 99.4\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6877528d-459e-4d87-b74b-f03a68efe54b",
   "metadata": {},
   "source": [
    "We can also use the return value of any filter to set values. For example, if we wanted to set all values greater than 4 to be 0 we can do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "e4ca47bc-ea89-45ca-8d46-8c9988482b31",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([0. , 2.1, 0. , 0. , 0. , 1.5, 0. , 3.7, 0. ])"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a[a > 4] = 0\n",
    "a"
   ]
  },
  {
   "cell_type": "raw",
   "id": "59f0e5d1-6a43-4771-94da-c7e3155f4b2b",
   "metadata": {},
   "source": [
    "<div class=\"operation\">\n",
    "<div>\n",
    "    <table class=\"array\"><tr><td>&nbsp;6.0</td></tr><tr><td>&nbsp;2.1</td></tr><tr><td>&nbsp;8.2</td></tr><tr><td>10.3</td></tr><tr><td>&nbsp;5.4</td></tr><tr><td>&nbsp;1.5</td></tr><tr><td>&nbsp;7.6</td></tr><tr><td>&nbsp;3.7</td></tr><tr><td>&nbsp;9.8</td></tr></table>\n",
    "</div>\n",
    "<div>[</div>\n",
    "<div>\n",
    "    <table class=\"array\"><tr><td>■</td></tr><tr><td>□</td></tr><tr><td>■</td></tr><tr><td>■</td></tr><tr><td>■</td></tr><tr><td>□</td></tr><tr><td>■</td></tr><tr><td>□</td></tr><tr><td>■</td></tr></table>\n",
    "</div>\n",
    "<div>]</div>\n",
    "<div>=</div>\n",
    "<div>0</div>\n",
    "<span style=\"font-size: 200%;\">⇨</span>\n",
    "<div>\n",
    "    <table class=\"array\"><tr><td>&nbsp;6.0</td></tr><tr><td class=\"empty\">&nbsp;2.1</td></tr><tr><td>&nbsp;8.2</td></tr><tr><td>10.3</td></tr><tr><td>&nbsp;5.4</td></tr><tr><td class=\"empty\">&nbsp;1.5</td></tr><tr><td>&nbsp;7.6</td></tr><tr><td class=\"empty\">&nbsp;3.7</td></tr><tr><td>&nbsp;9.8</td></tr></table>\n",
    "</div>\n",
    "<div>=</div>\n",
    "<div>0</div>\n",
    "<span style=\"font-size: 200%;\">⇨</span>\n",
    "<div>\n",
    "    <table class=\"array\"><tr><td>0.0</td></tr><tr><td>2.1</td></tr><tr><td>0.0</td></tr><tr><td>0.0</td></tr><tr><td>0.0</td></tr><tr><td>1.5</td></tr><tr><td>0.0</td></tr><tr><td>3.7</td></tr><tr><td>0.0</td></tr></table>\n",
    "</div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e25b3cf-edda-4842-85e4-1cbf6ca38935",
   "metadata": {},
   "source": [
    "## Extra: Filtering multi-dimensional data\n",
    "\n",
    "These techniques work with any dimensionality of data if setting values, but there are some issues to be aware of when dealing with higher-dimensions if you are extracting subsets. For example, let's take a 3D grid and select some values from it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "98f3662c-3488-48dd-986c-5887c129b10c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 2 3]\n",
      " [4 5 6]\n",
      " [7 8 9]]\n"
     ]
    }
   ],
   "source": [
    "grid = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])\n",
    "print(grid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "07bc896a-a186-4afc-b8bb-49dba3eaa34c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([5, 6, 7, 8, 9])"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "grid[grid > 4]"
   ]
  },
  {
   "cell_type": "raw",
   "id": "028c0e6e-7c60-467e-beb6-bba6ed1c93a4",
   "metadata": {},
   "source": [
    "<div class=\"operation\">\n",
    "<div>\n",
    "<table class=\"array\">\n",
    "<tbody>\n",
    "  <tr>\n",
    "    <td>1</td><td>2</td><td>3</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>4</td><td>5</td><td>6</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>7</td><td>8</td><td>9</td>\n",
    "  </tr>\n",
    "</tbody>\n",
    "</table>\n",
    "</div>\n",
    "<div>[</div>\n",
    "<div>\n",
    "<table class=\"array\">\n",
    "<tbody>\n",
    "  <tr>\n",
    "    <td>□</td><td>□</td><td>□</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>□</td><td>■</td><td>■</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>■</td><td>■</td><td>■</td>\n",
    "  </tr>\n",
    "</tbody>\n",
    "</table>\n",
    "</div>\n",
    "<div>]</div>\n",
    "<span style=\"font-size: 200%;\">⇨</span>\n",
    "<div>\n",
    "<table class=\"array\">\n",
    "<tbody>\n",
    "  <tr>\n",
    "    <td>5</td><td>6</td><td>7</td><td>8</td><td>9</td>\n",
    "  </tr>\n",
    "</tbody>\n",
    "</table>\n",
    "</div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d59a7cfc-df40-4304-8d2b-85d8a7291797",
   "metadata": {},
   "source": [
    "In this case, it has lost the information about *where* the numbers have come from. The dimensionality has been lost.\n",
    "\n",
    "NumPy hasn't much choice in this case, as if it were to return the result with the same shape as the original array `grid`, what should it put in the spaces that we've filtered out?\n",
    "\n",
    "```\n",
    "[[? ? ?]\n",
    " [? 5 6]\n",
    " [7 8 9]]\n",
    "```\n",
    "\n",
    "You might think that it could fill those with `0`, or `-1` but any of those could very easily cause a problem with code that follows. NumPy doesn't take a stance on this as it would be dangerous.\n",
    "\n",
    "In your code, you know what you're doing with your data, so it's ok for you to decide on a case-by-case basis. If you decide that you want to keep the original shape, but replace any filtered-out values with a `0` then you can use NumPy's [`where`](https://numpy.org/doc/stable/reference/generated/numpy.where.html) function. It takes three arguments:\n",
    "1. the boolean array selecting the values,\n",
    "2. an array or values to use in the spots you're keeping, and\n",
    "3. an array or values to use in the spots you're filtering out.\n",
    "\n",
    "So, in the case where we want to replace any values less-than or equal-to 4 with `0`, we can use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "3ff520ad-8657-4195-9baf-c073080b4baa",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0, 0, 0],\n",
       "       [0, 5, 6],\n",
       "       [7, 8, 9]])"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.where(grid > 4, grid, 0)"
   ]
  },
  {
   "cell_type": "raw",
   "id": "0d895aef-67a5-4460-b1cd-67d050b38c57",
   "metadata": {},
   "source": [
    "<div class=\"operation\">\n",
    "<div>np.where(</div>\n",
    "<div>\n",
    "<table class=\"array\">\n",
    "<tbody>\n",
    "  <tr>\n",
    "    <td>□</td><td>□</td><td>□</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>□</td><td>■</td><td>■</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>■</td><td>■</td><td>■</td>\n",
    "  </tr>\n",
    "</tbody>\n",
    "</table>\n",
    "</div>\n",
    "<div>,</div>\n",
    "<div>\n",
    "<table class=\"array\">\n",
    "<tbody>\n",
    "  <tr>\n",
    "    <td>1</td><td>2</td><td>3</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>4</td><td>5</td><td>6</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>7</td><td>8</td><td>9</td>\n",
    "  </tr>\n",
    "</tbody>\n",
    "</table>\n",
    "</div>\n",
    "<div>,</div>\n",
    "<div>0</div>\n",
    "<div>)</div>\n",
    "<span style=\"font-size: 200%;\">⇨</span>\n",
    "<div>\n",
    "<table class=\"array\">\n",
    "<tbody>\n",
    "  <tr>\n",
    "    <td>0</td><td>0</td><td>0</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>0</td><td>5</td><td>6</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>7</td><td>8</td><td>9</td>\n",
    "  </tr>\n",
    "</tbody>\n",
    "</table>\n",
    "</div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75577075-a2a6-450a-9792-3f2835fd70d7",
   "metadata": {},
   "source": [
    "Note that this has not affected the original array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "2f29e105-ffad-4908-b2c5-c8ee51b11081",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[1, 2, 3],\n",
       "       [4, 5, 6],\n",
       "       [7, 8, 9]])"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "grid"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26117bbb-6ccb-4538-9948-dd98ec97fb16",
   "metadata": {},
   "source": [
    "One final way that you can reduce your data, while keeping the dimensionality is to use [masked arrays](https://numpy.org/doc/stable/reference/maskedarray.html). This is useful in situations here you have *missing data*. The advantage of masked arrays is that operations like averaging are not affected by the cells that are masked out. The downside is that for much larger arrays they will use more memory and can be slower to operate on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "9dd96aad-cc87-4dc6-99e3-6e665bb786df",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[-- -- --]\n",
      " [-- 5 6]\n",
      " [7 8 9]]\n"
     ]
    }
   ],
   "source": [
    "masked_grid = np.ma.masked_array(grid, grid <= 4)\n",
    "print(masked_grid)"
   ]
  },
  {
   "cell_type": "raw",
   "id": "c617c74c-8483-408c-a102-a8716240f151",
   "metadata": {},
   "source": [
    "<div class=\"operation\">\n",
    "<div>np.ma.masked_array(</div>\n",
    "<div>\n",
    "<table class=\"array\">\n",
    "<tbody>\n",
    "  <tr>\n",
    "    <td>1</td><td>2</td><td>3</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>4</td><td>5</td><td>6</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>7</td><td>8</td><td>9</td>\n",
    "  </tr>\n",
    "</tbody>\n",
    "</table>\n",
    "</div>\n",
    "<div>,</div>\n",
    "<div>\n",
    "<table class=\"array\">\n",
    "<tbody>\n",
    "  <tr>\n",
    "    <td>■</td><td>■</td><td>■</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>■</td><td>□</td><td>□</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>□</td><td>□</td><td>□</td>\n",
    "  </tr>\n",
    "</tbody>\n",
    "</table>\n",
    "</div>\n",
    "<div>)</div>\n",
    "<span style=\"font-size: 200%;\">⇨</span>\n",
    "<div>\n",
    "<table class=\"array\">\n",
    "<tbody>\n",
    "  <tr>\n",
    "    <td class=\"empty\">&nbsp;</td><td class=\"empty\">&nbsp;</td><td class=\"empty\">&nbsp;</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td class=\"empty\">&nbsp;</td><td>5</td><td>6</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>7</td><td>8</td><td>9</td>\n",
    "  </tr>\n",
    "</tbody>\n",
    "</table>\n",
    "</div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "fda35344-eb25-40ba-8118-2df2a3f73d80",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "7.0"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.mean(masked_grid)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73d0457f-3958-471b-90af-5fe713670fb0",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "The `rain` data set represents the prediction of metres of rainfall in an area and is based on the data from [ECMWF](https://apps.ecmwf.int/codes/grib/param-db/?id=228). It is two-dimensional with axes of latitude and longitude.\n",
    "\n",
    "```python\n",
    "with np.load(\"weather_data.npz\") as weather:\n",
    "    rain = weather[\"rain\"]\n",
    "    uk_mask = weather[\"uk\"]\n",
    "    irl_mask = weather[\"ireland\"]\n",
    "    spain_mask = weather[\"spain\"]\n",
    "```\n",
    "\n",
    "- Calculate the mean of the entire 2D `rain` data set.\n",
    "- Look at the `uk_mask` array, including its `dtype` and `shape`\n",
    "- Filter the `rain` data set to contain only those values from within the UK.\n",
    "  - Does `[]` indexing, `np.where` or `masked_arrays` make the most sense for this task?\n",
    "- Calculate the mean (and maximum and minimum if you like) of the data\n",
    "- Do the same with Ireland and Spain and compare the numbers\n",
    "\n",
    "[<small>answer</small>](answer_filtering_weather.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
